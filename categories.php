<?php require_once 'includes/header.php'; ?>
<?php include('modal/categoriesModal.php');?>

<div class="row">
	<div class="col-md-12">

		<ol class="breadcrumb">
		  <li><a href="dashboard.php">Inicio</a></li>		  
		  <li class="active">Grupos</li>
		</ol>

		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="page-heading"> <i class="glyphicon glyphicon-edit"></i> Listado de grupos</div>
			</div> <!-- /panel-heading -->
			<div class="panel-body">
                            <div class="col-sm-5">
                                <div class="remove-messages"></div>

				<div class="div-action pull pull-right" style="padding-bottom:20px;">
					<button class="btn btn-default button1 addCategoriesModalBtn" data-toggle="modal" type="p" data-target="#addCategoriesModal"> <i class="glyphicon glyphicon-plus-sign"></i> Agregar grupo</button>
				</div> <!-- /div-action -->				
				
				<table class="table" id="manageCategoriesPadreTable">
					<thead>
						<tr>							
							<th>Nombre grupo</th>
							<th style="width:15%;">Opciones</th>
						</tr>
					</thead>
				</table>
				<!-- /table -->
                            </div>
				
                            <div class="col-sm-6 col-sm-offset-1">
                                <div class="remove-messages"></div>

				<div class="div-action pull pull-right" style="padding-bottom:20px;">
					<button class="btn btn-default button1 addCategoriesModalBtn" data-toggle="modal" type="h" data-target="#addCategoriesModal"> <i class="glyphicon glyphicon-plus-sign"></i> Agregar subgrupo</button>
				</div> <!-- /div-action -->				
				
				<table class="table" id="manageCategoriesTable">
					<thead>
						<tr>							
							<th>Nombre subgrupo</th>
							<th>Grupo Padre</th>
							<th style="width:15%;">Opciones</th>
						</tr>
					</thead>
				</table>
				<!-- /table -->
                            </div>
			</div> <!-- /panel-body -->
		</div> <!-- /panel -->		
	</div> <!-- /col-md-12 -->
</div> <!-- /row -->





<script src="custom/js/categories.js"></script>

<?php require_once 'includes/footer.php'; ?>
<?php 	

require_once 'core.php';

$valid['success'] = array('success' => false, 'messages' => array(), 'order_id' => '');
// print_r($valid);
if($_POST) {	
    
    $orderDate 		= isset($_POST['orderDate']) ? $_POST['orderDate'] : date('Y-m-d');
    $clientName 	= $_POST['clientName'];
    
    $paymentType 	= $_POST['paymentType'];
    $paymentStatus 	= $_POST['paymentStatus'];
    $discount 		= isset($_POST['discount']) ? (float)$_POST['discount'] : 0;//descuento
    $paid 		= $_POST['paid'];//pagado
    $factura 		= isset($_POST['numeroFactura']) ? $_POST['numeroFactura'] : '';//pagado
    
    $subTotalValue 	= 0;//subotal
    $items = array();
    $products = $_POST['productName'];
    if(count($_POST['productName'])>0){
        foreach( $_POST['productName'] as $index => $data){
            if($data!=''){
                $precio= isset($_POST['rate'][$index]) ? (float)$_POST['rate'][$index] : 0;
                $cantidad = isset($_POST['quantity'][$index]) ? (float)$_POST['quantity'][$index] : 0;
                $subTotalValue += $precio*$cantidad;
                $items[] = ['producto'=>$data,'precio'=>$precio,'cantidad'=>$cantidad,'total'=>$precio*$cantidad];
            }
        }
    }
    $vatValue = $subTotalValue * 0.12;
    $totalAmountValue = $vatValue+ $subTotalValue;
    $grandTotalValue = $totalAmountValue-$discount;/*pagototal*/
    $dueValue = $grandTotalValue-$paid;
    
    $sql = "INSERT INTO orders (order_date, client_name, sub_total, vat, total_amount, discount, grand_total, paid, due, payment_type, payment_status, order_status,factura) VALUES ('$orderDate', '$clientName', '$subTotalValue', '$vatValue', '$totalAmountValue', '$discount', '$grandTotalValue', '$paid', '$dueValue', $paymentType, $paymentStatus, 1, '$factura')";
	
	
	$order_id = 0;
	$orderStatus = false;
	if($connect->query($sql) === true) {
		$order_id = $connect->insert_id;
		$valid['order_id'] = $order_id;	
		$orderStatus = true;
	}

		
	// echo $_POST['productName'];
	$orderItemStatus = false;

        foreach($items as $item){
            $updateProductTable = "UPDATE product SET quantity = quantity - ".$item['cantidad']." WHERE product_id = ".$item['producto']."";
            $connect->query($updateProductTable);

            // add into order_item
            $orderItemSql = "INSERT INTO order_item (order_id, product_id, quantity, rate, total, order_item_status) 
            VALUES ('$order_id', '".$item['producto']."', '".$item['cantidad']."', '".$item['precio']."', '".$item['total']."', 1)";

            $connect->query($orderItemSql);	
        }

	$valid['success'] = true;
	$valid['messages'] = "Agregado exitosamente";		
	
	$connect->close();

	echo json_encode($valid);
 
} // /if $_POST
// echo json_encode($valid);
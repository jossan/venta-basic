<?php 	

require_once 'core.php';


$valid['success'] = array('success' => false, 'messages' => array());

$clienteId = isset($_POST['clienteId']) ?$_POST['clienteId'] : 0 ;
if($clienteId) { 

 $sql = "delete from clientes WHERE id = $clienteId";

 if($connect->query($sql) === TRUE) {
 	$valid['success'] = true;
	$valid['messages'] = "Eliminado exitosamente";		
 } else {
 	$valid['success'] = false;
 	$valid['messages'] = "Error no se ha podido eliminar";
 }
 
 $connect->close();

 echo json_encode($valid);
 
} // /if $_POST
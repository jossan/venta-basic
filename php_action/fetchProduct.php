<?php 	



require_once 'core.php';

$sql = "SELECT product.product_id, 
            product.product_name, 
            product.product_image, 
            product.brand_id,
            product.categories_id, 
            product.quantity, 
            product.rate, 
            product.active, 
            product.status, 
            brands.brand_name, 
            categories.categories_name,
            marca,
            descripcion,
            referencia,
            stockminimo,
            preciocompra,
            categories.parent_id,
            product.codigo as codigo
            FROM product 
		INNER JOIN brands ON product.brand_id = brands.brand_id 
		INNER JOIN categories ON product.categories_id = categories.categories_id  
		WHERE product.status = 1";

$result = $connect->query($sql);

$output = array('data' => array());

if($result->num_rows > 0) { 

 // $row = $result->fetch_array();
 $active = ""; 

 while($row = $result->fetch_array()) {
 	$productId = $row[0];
 	// active 
 	if($row[7] == 1) {
 		// activate member
 		$active = "<label class='label label-success'>Disponible</label>";
 	} else {
 		// deactivate member
 		$active = "<label class='label label-danger'>No disponible</label>";
 	} // /else

 	$button = '<!-- Single button -->
	<div class="btn-group">
	  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	    Acción <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu">
	    <li><a type="button" data-toggle="modal" id="editProductModalBtn" data-target="#editProductModal" onclick="editProduct('.$productId.')"> <i class="glyphicon glyphicon-edit"></i> Editar</a></li>
	    <li><a type="button" data-toggle="modal" data-target="#removeProductModal" id="removeProductModalBtn" onclick="removeProduct('.$productId.')"> <i class="glyphicon glyphicon-trash"></i> Eliminar</a></li>       
	  </ul>
	</div>';

	// $brandId = $row[3];
	// $brandSql = "SELECT * FROM brands WHERE brand_id = $brandId";
	// $brandData = $connect->query($sql);
	// $brand = "";
	// while($row = $brandData->fetch_assoc()) {
	// 	$brand = $row['brand_name'];
	// }

        $grupopadre = '';
        if($row[16]!=null){
            $sqlPadre = "select categories_name from categories where categories_id = $row[16]";
            $rowPadre= $connect->query($sqlPadre);
            $resultPadre = $rowPadre->fetch_array();
            $grupopadre = $resultPadre[0];
        }
        
	$brand = $row[9];
	$category = $row[10];
        $nameProduct = (($row[5]-$row[14])<=0 && $row[14]>0) ? '<span class="glyphicon glyphicon-info-sign text-danger"></span> '.$row[1]: $row[1];

        if($row[2]!==''){
            $imageUrl = substr($row[2], 3);
            $productImage = "<img class='img-round' src='".$imageUrl."' style='height:30px; width:50px;'  />";
        }else{
            $productImage = '';
        }
 	$output['data'][] = array( 	
 		// image
 		$productImage,
                //codigo
                $row['codigo'],
 		// product name
 		$nameProduct, 
 		// rate
 		$row[6],
 		// quantity 
 		$row[5], 
                //marca
                $row[11],
                // category 		
 		$category.'/'.$grupopadre,
 		// brand
 		$brand,
 		//descripcion
                $row[12],
                //referencia
//                $row[13],
                //stock minimo
                $row[14],
                //preciocompra
                $row[15],
 		// button
 		$button 		
 		); 	
 } // /while 

}// if num_rows

$connect->close();

echo json_encode($output);
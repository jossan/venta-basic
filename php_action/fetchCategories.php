<?php 	

require_once 'core.php';

$sql = "SELECT categories_id, categories_name, parent_id, categories_status FROM categories WHERE categories_status = 1 and parent_id>0";
$result = $connect->query($sql);

$output = array('data' => array());

if($result->num_rows > 0) { 

 // $row = $result->fetch_array();
 $activeCategories = ""; 

 while($row = $result->fetch_array()) {
 	$categoriesId = $row[0];
 	// active 
 	if($row[2] == '0') {
 		$parent = '';
 	} else {
                $sqlParent = "SELECT categories_name FROM categories WHERE categories_id = ".$row[2];
                $resultParent = $connect->query($sqlParent);
                $parentFetch = $resultParent->num_rows > 0 ? $resultParent->fetch_array() : array('') ;
 		$parent = $parentFetch[0];
 	}

 	$button = '<!-- Single button -->
	<div class="btn-group">
	  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	    Acción <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu">
	    <li><a type="button" data-toggle="modal" id="editCategoriesModalBtn" data-target="#editCategoriesModal" onclick="editCategories('.$categoriesId.',\'h\')"> <i class="glyphicon glyphicon-edit"></i> Editar</a></li>
	    <li><a type="button" data-toggle="modal" data-target="#removeCategoriesModal" id="removeCategoriesModalBtn" onclick="removeCategories('.$categoriesId.')"> <i class="glyphicon glyphicon-trash"></i> Eliminar</a></li>       
	  </ul>
	</div>';

 	$output['data'][] = array( 		
 		$row[1], 		
 		$parent,
 		$button 		
 	); 	
 } // /while 

}// if num_rows

$connect->close();

echo json_encode($output);
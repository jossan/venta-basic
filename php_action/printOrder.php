<?php 	

require_once 'core.php';

require_once '../vendor/autoload.php';

$orderId = $_GET['id'];


$sql = "SELECT order_date, client_name, client_contact, sub_total, vat, total_amount, discount, grand_total, paid, due , factura FROM orders WHERE order_id = $orderId";

$orderResult = $connect->query($sql);
$orderData = $orderResult->fetch_array();

$orderDate = $orderData[0];
$clientName = $orderData[1];
$clientContact = $orderData[2]; 
$subTotal = $orderData[3];
$vat = $orderData[4];
$totalAmount = $orderData[5]; 
$discount = $orderData[6];
$grandTotal = $orderData[7];
$paid = $orderData[8];
$due = $orderData[9];
$factura = $orderData[10];

$orderItemSql = "SELECT order_item.product_id, order_item.rate, order_item.quantity, order_item.total,
product.product_name FROM order_item
	INNER JOIN product ON order_item.product_id = product.product_id 
 WHERE order_item.order_id = $orderId";
$orderItemResult = $connect->query($orderItemSql);
$sqlCliente = "select * from clientes where id= $clientName";
$clienteResult = $connect->query($sqlCliente);
$cliente = $clienteResult->fetch_array();

 $table = '
 <style>
    td,th{
        padding:2px;
    }
    table{
        border-collapse: collapse;
    }
    .items{
        width:100%;
    }
    .items td,.items th {
        border: 0.1mm solid #555;
        font-size:11px;
        font-family:Arial;
        padding-left: 5px;
     }
    .items tr {
        border: 0.1mm solid #555;
     }
     .row th{
        background-color:#d2d2d2;
        font-weight: bold;
    }
    .numero{
        text-align:right;
        padding-right:5px;
    }
    .subitem{
        width:100%;
    }
    .subitem td{
        border: 0px;
    }
 </style>
 <table class="items">
	<thead>
            <tr>
                    <th rowspan="3" colspan="2">
                        <img src="../img/logo1.jpg" width="150px"><br>
                        <div style="color:#333;font-size:10px;">call1 mayo y calle 34</div>
                        <div style="color:#333;font-size:10px;">Teléfono: 0990189596</div>
                        <div style="color:#333;font-size:10px;">correo: jose@gmail.com</div>
                    </th>
                    <th colspan="2">'.$orderDate.'</th>
            </tr>
            <tr>
                    <th colspan="2">  N° FACTURA: </th>	
            </tr>
            <tr>
                    <th colspan="2"> '.$factura.' </th>	
            </tr>
            <tr><td colspan="2"><strong>Cliente :</strong> '.$cliente['nombre'].' '.$cliente['apellido'].'</td><td colspan="2"><strong>RUC/CI:</strong> '.$cliente['cedula'].'</td></tr>
            <tr><td colspan="2"><strong>Teléfono :</strong> '.$cliente['telefono'].'</td><td colspan="2"><strong>Correo:</strong> '.$cliente['correo'].'</td></tr>
            <tr><td  colspan="4" ><strong>Dirección :</strong>  '.$cliente['direccion'].'</td></tr>		
	</thead>

	<tbody>
		<tr class="row">
			<th>Producto</th>
                        <th>Cantidad</th>
			<th>Precio</th>
			<th>Total</th>
		</tr>';

		$x = 1;
		while($row = $orderItemResult->fetch_array()) {			
						
			$table .= '<tr>
				<th>'.$row[4].'</th>
                                <th>'.$row[2].'</th>
				<th class="numero">'.number_format($row[1],2).'</th>
				<th class="numero">'.number_format($row[3],2).'</th>
			</tr>
			';
		$x++;
		} // /while
		for($i = $x; $i<=7 ; $i++) {					
			$table .= '<tr>
				<th style="color:white;">0</th>
                                <th></th>
				<th class="numero"></th>
				<th class="numero"></th>
			</tr>';
		} // /while
		$table .= '
                    <tr><th colspan="4"></th></tr>
                    <tr>
                        <th colspan="2"  style="background-color:#f0f0f0;"></th>
			<th style="background-color:#f0f0f0;">Sub total</th>
			<th class="numero">'.number_format($subTotal,2).'</th>			
		</tr>

		<tr>
                        <th colspan="2"  style="background-color:#f0f0f0;"></th>
			<th style="background-color:#f0f0f0;">IVA (12%)</th>
			<th class="numero">'.number_format($vat,2).'</th>			
		</tr>

		<tr>
                        <th colspan="2" style="background-color:#f0f0f0;"></th>
			<th style="background-color:#f0f0f0;">Total neto</th>
			<th class="numero">'.number_format($totalAmount,2).'</th>			
		</tr>	

		<tr>
                        <th colspan="2" style="background-color:#f0f0f0;"></th>
			<th style="background-color:#f0f0f0;">Pagado</th>
			<th class="numero">'.number_format($paid,2).'</th>			
		</tr>

		<tr>
                        <th colspan="2" style="background-color:#f0f0f0;"></th>
			<th style="background-color:#f0f0f0;">Pendiente</th>
			<th class="numero">'.number_format($due,2).'</th>			
		</tr>
	</tbody>
</table>';

$connect->close();


$mpdf = new mPDF('utf-8', 'A5', 0, '', 10, 10, 12, 12, 5, 5, 'P');
$mpdf->WriteHTML($table);
$mpdf->Output('venta_'.$orderId.'.pdf','I');
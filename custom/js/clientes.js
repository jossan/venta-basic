var manageProductTable;

$(document).ready(function() {
	// top nav bar 
	$('#navCliente').addClass('active');
	// manage product data table
	manageProductTable = $('#manageProductTable').DataTable({
		'ajax': 'php_action/fetchCliente.php',
		'order': []
	});

	// add product modal btn clicked
	$("#addProductModalBtn").unbind('click').bind('click', function() {
		// // product form reset
		$("#submitProductForm")[0].reset();		

		// remove text-error 
		$(".text-danger").remove();
		// remove from-group error
		$(".form-group").removeClass('has-error').removeClass('has-success');

		$("#productImage").fileinput({
	      overwriteInitial: true,
		    maxFileSize: 2500,
		    showClose: false,
		    showCaption: false,
		    browseLabel: '',
		    removeLabel: '',
		    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
		    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
		    removeTitle: 'Cancel or reset changes',
		    elErrorContainer: '#kv-avatar-errors-1',
		    msgErrorClass: 'alert alert-block alert-danger',
		    defaultPreviewContent: '<img src="assests/images/photo_default.png" alt="Profile Image" style="width:100%;">',
		    layoutTemplates: {main2: '{preview} {remove} {browse}'},								    
	  		allowedFileExtensions: ["jpg", "png", "gif", "JPG", "PNG", "GIF"]
			});   

		// submit product form
		$("#submitProductForm").unbind('submit').bind('submit', function() {

				// submit loading button
				$("#createProductBtn").button('loading');

				var form = $(this);
				var formData = new FormData(this);

				$.ajax({
					url : form.attr('action'),
					type: form.attr('method'),
					data: formData,
					dataType: 'json',
					cache: false,
					contentType: false,
					processData: false,
					success:function(response) {

						if(response.success == true) {
							// submit loading button
							$("#createProductBtn").button('reset');
							
							$("#submitProductForm")[0].reset();

							$("html, body, div.modal, div.modal-content, div.modal-body").animate({scrollTop: '0'}, 100);
																	
							// shows a successful message after operation
							$('#add-product-messages').html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

							// remove the mesages
		          $(".alert-success").delay(500).show(10, function() {
								$(this).delay(3000).hide(10, function() {
									$(this).remove();
								});
							}); // /.alert

		          // reload the manage student table
							manageProductTable.ajax.reload(null, true);

							// remove text-error 
							$(".text-danger").remove();
							// remove from-group error
							$(".form-group").removeClass('has-error').removeClass('has-success');

						} // /if response.success
						
					} // /success function
				}); // /ajax function

			return false;
		}); // /submit product form

	}); // /add product modal btn clicked
	

	// remove product 	

}); // document.ready fucntion

function editCliente(clienteId = null) {

	if(clienteId) {
		$('#editClienteForm')[0].reset();
                
		$.ajax({
			url: 'php_action/fetchSelectedCliente.php',
			type: 'post',
			data: {clienteId: clienteId},
			dataType: 'json',
			success:function(response) {		
			// alert(response.product_image);
				// modal spinner
				$('.div-loading').addClass('div-hide');
				// modal div
				$('.div-result').removeClass('div-hide');				

				// product name
                                $("#editId").val(response.id);
				$("#editNombre").val(response.nombre);
                                $("#editApellido").val(response.apellido);
                                $("#editCedula").val(response.cedula);
                                $("#editTelefono").val(response.telefono);
                                $("#editCorreo").val(response.correo);
                                $("#editDireccion").val(response.direccion);
	

				// update the product data function
				$("#editClienteForm").unbind('submit').bind('submit', function() {

                                    // submit loading button
                                    $("#editClienteBtn").button('loading');

                                    var form = $(this);
                                    var formData = new FormData(this);

                                    $.ajax({
                                            url : form.attr('action'),
                                            type: form.attr('method'),
                                            data: formData,
                                            dataType: 'json',
                                            cache: false,
                                            contentType: false,
                                            processData: false,
                                            success:function(response) {
                                                    if(response.success == true) {
                                                            // submit loading button
                                                            $("#editClienteBtn").button('reset');																		

                                                            $("html, body, div.modal, div.modal-content, div.modal-body").animate({scrollTop: '0'}, 100);

                                                            // shows a successful message after operation
                                                            $('#edit-product-messages').html('<div class="alert alert-success">'+
                                '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                                '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
                              '</div>');

                                                            // remove the mesages
                              $(".alert-success").delay(500).show(10, function() {
                                                                    $(this).delay(3000).hide(10, function() {
                                                                            $(this).remove();
                                                                    });
                                                            }); // /.alert

                              // reload the manage student table
                                                            manageProductTable.ajax.reload(null, true);

                                                            // remove text-error 
                                                            //$(".text-danger").remove();
                                                            // remove from-group error
                                                            //$(".form-group").removeClass('has-error').removeClass('has-success');
                                                            $('#editClienteModal').modal('hide');
                                                    } // /if response.success

                                            } // /success function
                                    }); // /ajax function

					return false;
				}); // update the product data function

			} // /success function
		}); // /ajax to fetch product image
				
	} else {
		alert('error please refresh the page');
	}
} // /edit product function

// remove product 
function removeCliente(clienteId = null) {
	if(clienteId) {
		// remove product button clicked
		//$("#removeClienteBtn").unbind('click').bind('click', function() {
			// loading remove button
			$("#removeClienteBtn").button('loading');
			$.ajax({
				url: 'php_action/removeCliente.php',
				type: 'post',
				data: {clienteId: clienteId},
				dataType: 'json',
				success:function(response) {
					// loading remove button
					$("#removeClienteBtn").button('reset');
					if(response.success == true) {
						// remove product modal
						$("#removeProductModal").modal('hide');

						// update the product table
						manageProductTable.ajax.reload(null, false);

						// remove success messages
						$(".remove-messages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						// remove the mesages
	          $(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert
					} else {

						// remove success messages
						$(".removeClienteMessages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						// remove the mesages
	          $(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert

					} // /error
				} // /success function
			}); // /ajax fucntion to remove the product
			return false;
		//}); // /remove product btn clicked
	} // /if productid
} // /remove product function

function clearForm(oForm) {
	// var frm_elements = oForm.elements;									
	// console.log(frm_elements);
	// 	for(i=0;i<frm_elements.length;i++) {
	// 		field_type = frm_elements[i].type.toLowerCase();									
	// 		switch (field_type) {
	// 	    case "text":
	// 	    case "password":
	// 	    case "textarea":
	// 	    case "hidden":
	// 	    case "select-one":	    
	// 	      frm_elements[i].value = "";
	// 	      break;
	// 	    case "radio":
	// 	    case "checkbox":	    
	// 	      if (frm_elements[i].checked)
	// 	      {
	// 	          frm_elements[i].checked = false;
	// 	      }
	// 	      break;
	// 	    case "file": 
	// 	    	if(frm_elements[i].options) {
	// 	    		frm_elements[i].options= false;
	// 	    	}
	// 	    default:
	// 	        break;
	//     } // /switch
	// 	} // for
}
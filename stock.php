<?php require_once 'php_action/db_connect.php' ?>
<?php require_once 'includes/header.php'; ?>
<?php require_once 'modal/productModal.php'; ?>
<?php $bajo = isset($_GET['bajo']) ? true : false;?>
<div class="row">
	<div class="col-md-12">

		<ol class="breadcrumb">
		  <li><a href="dashboard.php">Inicio</a></li>
                  <li><a href="product.php">Productos</a></li>	
		  <li class="active">Stock</li>
		</ol>

		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="page-heading"> <i class="glyphicon glyphicon-edit"></i> Listado de productos</div>
			</div> <!-- /panel-heading -->
			<div class="panel-body">
                            <form action="php_action/saveStock.php" method="post">
                                <?php if($bajo==true): ?>
                                    <div><button href="stock.php" class="btn btn-info"><span class="glyphicon glyphicon-eye-open"></span> ver stock de todos los productos</button></div>
                                    <div class="text-center"><h3>Invetario de productos con stock bajo</h3></div>
                                <?php else: ?>
                                    <div><a href="stock.php?bajo=1" class="btn btn-danger"><span class="glyphicon glyphicon-eye-open"></span> ver productos con stock bajo</a></div>
                                    <div class="text-center"><h3>Invetario de todos los productos</h3></div>
                                <?php endif; ?>
                                <span class="pull-right"><button type="submit" class="btn btn-primary">Guardar >></button></span>
                                
                                <table class="table">
                                    <thead><th></th><th>Producto</th><th>Stock mínimo</th><th>Stock actual</th><th>Nuevo stock</th></thead>
                                    <tbody>
                                    <?php 
                                        if($bajo){
                                            $sqlP = 'select product_id, product_image, product_name, stockminimo, quantity from product where stockminimo >= quantity  AND status = 1';
                                        }else{
                                            $sqlP = 'select product_id, product_image, product_name, stockminimo, quantity from product  where status = 1';
                                        }
                                        $resultP = $connect->query($sqlP);
                                        $index = 0;
                                        while($rowP = $resultP->fetch_array()) { 
                                            $estadoStock = ($rowP['quantity']-$rowP['stockminimo'])<=0 ? 'style="background-color:#EFD8D8;"':'';
                                            if($rowP['product_image']!==''){
                                                $imageUrl = substr($rowP['product_image'], 3);
                                                $productImage = "<img class='img-round' src='".$imageUrl."' style='height:30px; width:50px;'  />";
                                            }else{
                                                $productImage = '';
                                            }
                                            echo '<tr '.$estadoStock.'>';
                                            echo '<td>'.$productImage.'</td>';
                                            echo '<td>'.$rowP['product_name'].'</td>';
                                            echo '<td>'.$rowP['stockminimo'].'</td>';
                                            echo '<td><input type="hidden" value="'.$rowP['product_id'].'" name="id['.$index.']"> '.$rowP['quantity'].'</td>';
                                            echo '<td><input type="number" value="0" class="form-control" name="stock['.$index.']" style="width:100px;"></td>';
                                            echo '</tr>';
                                            $index++;
                                        } // if num_rows
                                    ?>
                                    </tbody>
                                </table>
                                <hr>
                                <span class="pull-right"><button type="submit" class="btn btn-primary">Guardar >></button></span>
                            </form>
			</div> <!-- /panel-body -->
		</div> <!-- /panel -->		
	</div> <!-- /col-md-12 -->
</div> <!-- /row -->






<script src="custom/js/product.js"></script>

<?php require_once 'includes/footer.php'; ?>
<!-- add cliente -->
<div class="modal fade" id="addClienteModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">

    	<form class="form-horizontal" id="submitProductForm" action="php_action/createCliente.php" method="POST">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-plus"></i> Agregar cliente </h4>
	      </div>

	      <div class="modal-body" style="max-height:450px; overflow:auto;">

	      	<div id="add-product-messages"></div>

	      	    	           	       
                <div class="form-group">
	        	<label for="quantity" class="col-sm-3 control-label">Nombres: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
                                        <input type="text" class="form-control" required="required" id="nombre" placeholder="Nombres Completos" name="nombre" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->	
                
	        <div class="form-group">
	        	<label for="productName" class="col-sm-3 control-label">Apellidos: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="apellido" placeholder="Apellidos Completos" name="apellido" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->	    

	                	 

	        <div class="form-group">
	        	<label for="rate" class="col-sm-3 control-label">Cedula o RUC: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="cedula" placeholder="Cedula o RUC" name="cedula" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->	     	        

	        <div class="form-group">
	        	<label for="brandName" class="col-sm-3 control-label">Teléfono: </label>
	        	<label class="col-sm-1 control-label">: </label>
	        	<div class="col-sm-8">
				      <input type="text" class="form-control" id="telefono" placeholder="Teléfono" name="telefono" autocomplete="off">
				    </div>
				   
	        </div> <!-- /form-group-->	
	         <div class="form-group">
	        	<label for="brandName" class="col-sm-3 control-label">Correo: </label>
	        	<label class="col-sm-1 control-label">: </label>
	        	<div class="col-sm-8">
				      <input type="text" class="form-control" id="correo" placeholder="Correo" name="correo" autocomplete="off">
				    </div>
				   
	        </div> <!-- /form-group-->

	         <div class="form-group">
	        	<label for="brandName" class="col-sm-3 control-label">Dirección: </label>
	        	<label class="col-sm-1 control-label">: </label>
	        	<div class="col-sm-8">
				      <input type="text" class="form-control" id="direccion" placeholder="Dirección" name="direccion" autocomplete="off">
				    </div>
				   
	        </div> <!-- /form-group-->
	             	        
	      </div> <!-- /modal-body -->
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Cerrar</button>
	        
	        <button type="submit" class="btn btn-primary" id="createProductBtn" data-loading-text="Loading..." autocomplete="off"> <i class="glyphicon glyphicon-ok-sign"></i> Guardar cambios</button>
	      </div> <!-- /modal-footer -->	      
     	</form> <!-- /.form -->	     
    </div> <!-- /modal-content -->    
  </div> <!-- /modal-dailog -->
</div> 
<!-- /add cliente -->


<!-- edit categories -->
<div class="modal fade" id="editClienteModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">

    	<form class="form-horizontal" id="editClienteForm" action="php_action/editCliente.php" method="POST">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-pencil"></i> Editar cliente </h4>
	      </div>

	      <div class="modal-body" style="max-height:450px; overflow:auto;">

	      	<div id="add-cliente-messages"></div>

                <div class="modal-loading div-hide" style="width:50px; margin:auto;padding-top:50px; padding-bottom:50px;">
						<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
						<span class="sr-only">Cargando...</span>
					</div>
                
                <input type="hidden" required="required" id="editId" name="id">
                <div class="form-group">
	        	<label for="quantity" class="col-sm-3 control-label">Nombres: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
                                        <input type="text" class="form-control" required="required" id="editNombre" placeholder="Nombres Completos" name="nombre" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->	
                
	        <div class="form-group">
	        	<label for="productName" class="col-sm-3 control-label">Apellidos: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="editApellido" placeholder="Apellidos Completos" name="apellido" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->	    

	                	 

	        <div class="form-group">
	        	<label for="rate" class="col-sm-3 control-label">Cedula o RUC: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="editCedula" placeholder="Cedula o RUC" name="cedula" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->	     	        

	        <div class="form-group">
	        	<label for="brandName" class="col-sm-3 control-label">Teléfono: </label>
	        	<label class="col-sm-1 control-label">: </label>
	        	<div class="col-sm-8">
				      <input type="text" class="form-control" id="editTelefono" placeholder="Teléfono" name="telefono" autocomplete="off">
				    </div>
				   
	        </div> <!-- /form-group-->	
	         <div class="form-group">
	        	<label for="brandName" class="col-sm-3 control-label">Correo: </label>
	        	<label class="col-sm-1 control-label">: </label>
	        	<div class="col-sm-8">
				      <input type="text" class="form-control" id="editCorreo" placeholder="Correo" name="correo" autocomplete="off">
				    </div>
				   
	        </div> <!-- /form-group-->

	         <div class="form-group">
	        	<label for="brandName" class="col-sm-3 control-label">Dirección: </label>
	        	<label class="col-sm-1 control-label">: </label>
	        	<div class="col-sm-8">
				      <input type="text" class="form-control" id="editDireccion" placeholder="Dirección" name="direccion" autocomplete="off">
				    </div>
				   
	        </div> <!-- /form-group-->
	             	        
	      </div> <!-- /modal-body -->
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Cerrar</button>
	        
	        <button type="submit" class="btn btn-primary" id="editClienteBtn" data-loading-text="Loading..." autocomplete="off"> <i class="glyphicon glyphicon-ok-sign"></i> Guardar cambios</button>
	      </div> <!-- /modal-footer -->	      
     	</form> <!-- /.form -->	     
    </div> <!-- /modal-content -->    
  </div> <!-- /modal-dailog -->
</div> 
<!-- /edit cliente -->


<!-- categories brand -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeProductModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="glyphicon glyphicon-trash"></i> Eliminar producto</h4>
      </div>
      <div class="modal-body">

      	<div class="removeProductMessages"></div>

        <p>Realmente deseas eliminar el producto?</p>
      </div>
      <div class="modal-footer removeProductFooter">
        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Cancelar</button>
        <button type="button" class="btn btn-primary" id="removeProductBtn" data-loading-text="Loading..."> <i class="glyphicon glyphicon-ok-sign"></i> Eliminar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
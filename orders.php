<?php 
require_once 'php_action/db_connect.php'; 
require_once 'includes/header.php'; 
require_once 'modal/clienteModal.php'; 

if($_GET['o'] == 'add') { 
// add order
	echo "<div class='div-request div-hide'>add</div>";
} else if($_GET['o'] == 'manord') { 
	echo "<div class='div-request div-hide'>manord</div>";
} else if($_GET['o'] == 'editOrd') { 
	echo "<div class='div-request div-hide'>editOrd</div>";
} // /else manage order

$clientesSql = "SELECT id, concat(cedula,' | ',nombre, ' ', apellido ) as nombre FROM clientes order by nombre";
$clienteData = $connect->query($clientesSql);
$dataClienteArray = array(); 
if($clienteData->num_rows > 0) { 
    while($row = $clienteData->fetch_array()) {
        $dataClienteArray[] = ['id'=>$row[0],'text'=>$row[1]];
    }
}

$productSql = "SELECT * FROM product WHERE active = 1 AND status = 1";
$productData = $connect->query($productSql);
$dataProductArray = array();
while($row = $productData->fetch_array()) {									 		
        $dataProductArray[] = ['id'=>$row['product_id'],'text'=> $row['codigo'].' | '.$row['product_name'] ];
                //."' id='changeProduct".$row['product_id']."'>".$row['product_name']."</option>";
} // /while 
?>
<script type="text/javascript">
    var dataCliente = <?= json_encode($dataClienteArray);?>;
    var dataProduct = <?= json_encode($dataProductArray);?>;
</script>
<ol class="breadcrumb">
  <li><a href="dashboard.php">Inicio</a></li>
  <li>Ventas</li>
  <li class="active">
  	<?php if($_GET['o'] == 'add') { ?>
  		Agregar ventas
		<?php } else if($_GET['o'] == 'manord') { ?>
			Listado de ventas
		<?php } // /else manage order ?>
  </li>
</ol>


<h4>
	<i class='glyphicon glyphicon-circle-arrow-right'></i>
	<?php if($_GET['o'] == 'add') {
		echo "Agregar ventas";
	} else if($_GET['o'] == 'manord') { 
		echo "Listado de ventas";
	} else if($_GET['o'] == 'editOrd') { 
		echo "Editar ventas";
	}
	?>	
</h4>



<div class="panel panel-default">
	<div class="panel-heading">

		<?php if($_GET['o'] == 'add') { ?>
  		<i class="glyphicon glyphicon-plus-sign"></i>	Agregar ventas
		<?php } else if($_GET['o'] == 'manord') { ?>
			<i class="glyphicon glyphicon-edit"></i> Listado de ventas
		<?php } else if($_GET['o'] == 'editOrd') { ?>
			<i class="glyphicon glyphicon-edit"></i> Editar ventas
		<?php } ?>

	</div> <!--/panel-->	
	<div class="panel-body">
			
		<?php if($_GET['o'] == 'add') { 
			// add order
			?>			

			<div class="success-messages"></div> <!--/success-messages-->

  		<form class="form-horizontal" method="POST" action="php_action/createOrder.php" id="createOrderForm">

			  <div class="form-group">
			    <label for="orderDate" class="col-sm-2 control-label">Fecha de ventas</label>
			    <div class="col-sm-4">
			      <input type="text" class="form-control input-sm" id="orderDate" name="orderDate" autocomplete="off" value="<?= date('Y-m-d');?>"/>
			    </div>
                            <label for="numeroFactura" class="col-sm-2 control-label">Número de factura</label>
			    <div class="col-sm-4">
			      <input type="text" class="form-control input-sm" id="numeroFactura" name="numeroFactura" autocomplete="off" />
			    </div>
			  </div> <!--/form-group-->
			  <div class="form-group">
			    <label for="clientName" class="col-sm-2 control-label">Cliente</label>
			    <div class="col-sm-8">
                                <select class="cliente-select2" class="form-control" id="clientName" name="clientName" style="width: 100%">
                                    <?php foreach($dataClienteArray as $cliente):?>
                                        <option value="<?= $cliente['id']?>" <?= $cliente['id']=='1' ? 'selected': '';?> ><?= $cliente['text']?></option>
                                    <?php endforeach;?>
                                </select>
			    </div>
                            <div class="col-sm-2" ><button type="button" data-toggle="modal" data-target="#addClienteModal" class="btn btn-sm btn-block btn-primary"><span class="glyphicon glyphicon-user"></span> Agregar</button></div>
			  </div> <!--/form-group-->

                          <hr>
			  <table class="" id="productTable">
                              <thead style="background-color: #f0f0f0;" >
			  		<tr >	
                                                <th style="width:5%;">#</th>
			  			<th style="width:45%;">Producto</th>
			  			<th style="width:10%;">Precio</th>
			  			<th style="width:10%;">Cantidad</th>			  			
                                                <th style="width:10%;">Existencia</th>
			  			<th style="width:10%;">Total</th>			  			
                                                <th style="width:10%;" style="padding:2px;">
                                                    <button type="button" class="btn btn-info btn-block btn-xs" onclick="addRow()" id="addRowBtn" data-loading-text="Cargando..."> <i class="glyphicon glyphicon-plus-sign"></i> Añadir fila </button>
                                                </th>
			  		</tr>
			  	</thead>
			  	<tbody>
			  		<?php
			  		$arrayNumber = 0;
			  		for($x = 1; $x <= 7; $x++) { ?>
                                    <tr id="row<?php echo $x; ?>" class="<?php echo $arrayNumber; ?>" >
                                                    <td><?= $x?></td>
                                                        <td style="padding-top: 4px;">
                                                            <div class="form-group" style="margin:0px 10px 0px 0px;">
			  					<select class="form-control input-sm productos-items" name="productName[]" id="productName<?php echo $x; ?>" onchange="getProductData(<?php echo $x; ?>)" >
			  						<option value="">--</option>
		  						</select>
                                                            </div>
			  				</td>
			  				<td style="padding-top: 4px;">
                                                            <div class="form-group" style="margin:0px 10px 0px 0px;">
			  					<input type="text" name="rate[]" id="rate<?php echo $x; ?>" autocomplete="off" class="form-control input-sm" />			  					
			  					<input type="hidden" name="rateValue[]" id="rateValue<?php echo $x; ?>" />			  					
                                                            </div>
			  				</td>
			  				<td style="padding-top: 4px;">
			  					<div class="form-group" style="margin:0px 10px 0px 0px;">
                                                                    <input type="number" onchange="verificarExistencia();getTotal(<?php echo $x ?>);" name="quantity[]" id="quantity<?php echo $x; ?>" onkeyup="getTotal(<?php echo $x ?>)" autocomplete="off" class="form-control input-sm" min="1" />
			  					</div>
			  				</td>
                                                        <td style="padding-top: 4px;">
                                                            <div class="form-group" style="margin:0px 10px 0px 0px;">
                                                                <input type="number" disabled="disabled" name="existencia[]" id="existencia<?php echo $x; ?>" autocomplete="off" class="form-control input-sm" />
                                                            </div>
			  				</td>
			  				<td style="padding-top: 4px;">
                                                            <div class="form-group" style="margin:0px 10px 0px 0px;">
			  					<input type="text" name="total[]" id="total<?php echo $x; ?>" autocomplete="off" class="form-control input-sm" disabled="true" />
			  					<input type="hidden" name="totalValue[]" id="totalValue<?php echo $x; ?>"  />
                                                            </div>
			  				</td>
			  				<td style="padding-top: 4px;">
			  					<button class="btn btn-danger btn-sm removeProductRowBtn" type="button" id="removeProductRowBtn" onclick="removeProductRow(<?php echo $x; ?>)"><i class="glyphicon glyphicon-remove"></i></button>
			  				</td>
			  			</tr>
		  			<?php
		  			$arrayNumber++;
			  		} // /for
			  		?>
			  	</tbody>			  	
			  </table>
                          <hr>
                        <div class="col-sm-8 col-sm-offset-4">
			  <div class="col-md-6">
                              <div class="form-group" style="margin-bottom: 4px;">
                                    <label for="subTotal" class="col-sm-5 control-label" style="font-size:12px;">Sub total</label>
				    <div class="col-sm-7">
				      <input type="text" style="font-size:12px;" class="form-control input-sm" id="subTotal" name="subTotal" disabled="true" />
				      <input type="hidden" class="form-control" id="subTotalValue" name="subTotalValue" />
				    </div>
				  </div> <!--/form-group-->			  
				  <div class="form-group" style="margin-bottom: 4px;">
				    <label for="vat" style="font-size:12px;" class="col-sm-5 control-label">IVA 12%</label>
				    <div class="col-sm-7" >
				      <input type="text" class="form-control input-sm" id="vat" name="vat" disabled="true" />
				      <input type="hidden" id="vatValue" name="vatValue" />
				    </div>
				  </div> <!--/form-group-->			  
				  <div class="form-group" style="margin-bottom: 4px;">
				    <label for="totalAmount" style="font-size:12px;" class="col-sm-5 control-label">Total neto</label>
				    <div class="col-sm-7">
				      <input type="text" class="form-control input-sm" id="totalAmount" name="totalAmount" disabled="true"/>
				      <input type="hidden" class="form-control" id="totalAmountValue" name="totalAmountValue" />
				    </div>
				  </div> <!--/form-group-->			  
				  <div class="form-group" style="margin-bottom: 4px;display:none;">
				    <label for="discount" class="col-sm-5 control-label" style="font-size:12px;">Descuento</label>
				    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" value="0" id="discount" name="discount" onkeyup="discountFunc()" autocomplete="off" />
				    </div>
				  </div> <!--/form-group-->	
				  <div class="form-group" style="margin-bottom: 4px;">
				    <label for="grandTotal" class="col-sm-5 control-label" style="font-size:12px;">Total</label>
				    <div class="col-sm-7">
				      <input type="text" class="form-control input-sm" id="grandTotal" name="grandTotal" disabled="true" />
				      <input type="hidden" class="form-control" id="grandTotalValue" name="grandTotalValue" />
				    </div>
				  </div> <!--/form-group-->			  		  
			  </div> <!--/col-md-6-->

			  <div class="col-md-6">
			  	<div class="form-group" style="margin-bottom: 4px;">
				    <label for="paid" class="col-sm-5 control-label" style="font-size:12px;">Monto pagado</label>
				    <div class="col-sm-7">
				      <input type="text" class="form-control input-sm" id="paid" name="paid" autocomplete="off" onkeyup="paidAmount()" />
				    </div>
				  </div> <!--/form-group-->			  
				  <div class="form-group" style="margin-bottom: 4px;">
				    <label for="due" class="col-sm-5 control-label" style="font-size:12px;">Saldo</label>
				    <div class="col-sm-7">
				      <input type="text" class="form-control input-sm" id="due" name="due" disabled="true" />
				      <input type="hidden" class="form-control" id="dueValue" name="dueValue" />
				    </div>
				  </div> <!--/form-group-->		
				  <div class="form-group" style="margin-bottom: 4px;">
				    <label for="clientContact" class="col-sm-5 control-label" style="font-size:12px;">Método de pago</label>
				    <div class="col-sm-7">
				      <select class="form-control  input-sm" name="paymentType" id="paymentType">
				      	<option value="1">Cheque</option>
				      	<option value="2" selected="">Efectivo</option>
				      	<option value="3">Tarjeta de crédito</option>
				      </select>
				    </div>
				  </div> <!--/form-group-->							  
				  <div class="form-group" style="margin-bottom: 4px;">
				    <label for="clientContact" class="col-sm-5 control-label" style="font-size:12px;">Estado</label>
				    <div class="col-sm-7">
				      <select class="form-control input-sm" name="paymentStatus" id="paymentStatus">
                                        <option value="1" selected="">Pago completo</option>
				      	<option value="2">Pago por adelantado</option>
				      	<option value="3">No pagado</option>
				      </select>
				    </div>
				  </div> <!--/form-group-->							  
			  </div> <!--/col-md-6-->
                        </div>

			  <div class="form-group submitButtonFooter">
			    <div class="col-sm-offset-9 col-sm-3">

			      <button type="submit" id="createOrderBtn" data-loading-text="Cargando..." class="btn btn-success btn-block"><i class="glyphicon glyphicon-ok-sign"></i> Guardar cambios</button>
                              <div id="mensaje-error" class="text-danger text-center" style="display:none;">Cantidad es mayor a existencia</div>

			      <!--<button type="reset" class="btn btn-default" onclick="resetOrderForm()"><i class="glyphicon glyphicon-erase"></i> Reiniciar</button>-->
			    </div>
			  </div>
			</form>
		<?php } else if($_GET['o'] == 'manord') { 
			// manage order
			?>

			<div id="success-messages"></div>
			
			<table class="table" id="manageOrderTable">
				<thead>
					<tr>
						<th>#</th>
						<th>Fecha</th>
						<th>Cliente</th>
						<th>Teléfono</th>
						<th>Total de productos</th>
						<th>Estado del pago</th>
						<th>Opciones</th>
					</tr>
				</thead>
			</table>

		<?php 
		// /else manage order
		} else if($_GET['o'] == 'editOrd') {
			// get order
			?>
			
			<div class="success-messages"></div> <!--/success-messages-->

  		<form class="form-horizontal" method="POST" action="php_action/editOrder.php" id="editOrderForm">

  			<?php $orderId = $_GET['i'];

  			$sql = "SELECT orders.order_id, orders.order_date, orders.client_name, orders.client_contact, orders.sub_total, orders.vat, orders.total_amount, orders.discount, orders.grand_total, orders.paid, orders.due, orders.payment_type, orders.payment_status FROM orders 	
					WHERE orders.order_id = {$orderId}";

				$result = $connect->query($sql);
				$data = $result->fetch_row();				
  			?>

			  <div class="form-group">
			    <label for="orderDate" class="col-sm-2 control-label">Fecha de venta</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="orderDate" name="orderDate" autocomplete="off" value="<?php echo $data[1] ?>" />
			    </div>
			  </div> <!--/form-group-->
			  <div class="form-group">
			    <label for="clientName" class="col-sm-2 control-label">Nombre del cliente</label>
			    <div class="col-sm-10">
                                <select class="cliente-select2">
                                <option value="3620194" selected="selected">select2/select2</option>
                              </select>
			      <input type="text" class="form-control" id="clientName" name="clientName" placeholder="Cliente" autocomplete="off" value="<?php echo $data[2] ?>" />
			    </div>
			  </div> <!--/form-group-->
			  <div class="form-group">
			    <label for="clientContact" class="col-sm-2 control-label">Teléfono del cliente</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="clientContact" name="clientContact" placeholder="Teléfono" autocomplete="off" value="<?php echo $data[3] ?>" />
			    </div>
			  </div> <!--/form-group-->			  

			  <table class="table" id="productTable">
			  	<thead>
			  		<tr>			  			
			  			<th style="width:40%;">Producto</th>
			  			<th style="width:20%;">Precio</th>
			  			<th style="width:15%;">Cantidad</th>			  			
			  			<th style="width:15%;">Total</th>			  			
			  			<th style="width:10%;"></th>
			  		</tr>
			  	</thead>
			  	<tbody>
			  		<?php

			  		$orderItemSql = "SELECT order_item.order_item_id, order_item.order_id, order_item.product_id, order_item.quantity, order_item.rate, order_item.total FROM order_item WHERE order_item.order_id = {$orderId}";
						$orderItemResult = $connect->query($orderItemSql);
						// $orderItemData = $orderItemResult->fetch_all();						
						
						// print_r($orderItemData);
			  		$arrayNumber = 0;
			  		// for($x = 1; $x <= count($orderItemData); $x++) {
			  		$x = 1;
			  		while($orderItemData = $orderItemResult->fetch_array()) { 
			  			// print_r($orderItemData); ?>
			  			<tr id="row<?php echo $x; ?>" class="<?php echo $arrayNumber; ?>">			  				
			  				<td style="margin-left:20px;">
			  					<div class="form-group">

			  					<select class="form-control" name="productName[]" id="productName<?php echo $x; ?>" onchange="getProductData(<?php echo $x; ?>)" >
			  						<option value="">-- Selecciona --</option>
			  						<?php
			  							$productSql = "SELECT * FROM product WHERE active = 1 AND status = 1 AND quantity != 0";
			  							$productData = $connect->query($productSql);

			  							while($row = $productData->fetch_array()) {									 		
			  								$selected = "";
			  								if($row['product_id'] == $orderItemData['product_id']) {
			  									$selected = "selected";
			  								} else {
			  									$selected = "";
			  								}

			  								echo "<option value='".$row['product_id']."' id='changeProduct".$row['product_id']."' ".$selected." >".$row['product_name']."</option>";
										 	} // /while 

			  						?>
		  						</select>
			  					</div>
			  				</td>
			  				<td style="padding-left:20px;">			  					
			  					<input type="text" name="rate[]" id="rate<?php echo $x; ?>" autocomplete="off" disabled="true" class="form-control" value="<?php echo $orderItemData['rate']; ?>" onkeyup="getTotal(<?php echo $x; ?>)" onchange="getTotal(<?php echo $x; ?>)"/>			  					
			  					<input type="hidden" name="rateValue[]" id="rateValue<?php echo $x; ?>" autocomplete="off" class="form-control" value="<?php echo $orderItemData['rate']; ?>" />			  					
			  				</td>
			  				<td style="padding-left:20px;">
			  					<div class="form-group">
                                                                    <input type="number" name="quantity[]" id="quantity<?php echo $x; ?>" onkeyup="getTotal(<?php echo $x ?>)" autocomplete="off" class="form-control" min="1" value="<?php echo $orderItemData['quantity']; ?>" />
			  					</div>
			  				</td>
			  				<td style="padding-left:20px;">			  					
			  					<input type="text" name="total[]" id="total<?php echo $x; ?>" autocomplete="off" class="form-control" disabled="true" value="<?php echo $orderItemData['total']; ?>"/>			  					
			  					<input type="hidden" name="totalValue[]" id="totalValue<?php echo $x; ?>" autocomplete="off" class="form-control" value="<?php echo $orderItemData['total']; ?>"/>			  					
			  				</td>
			  				<td>

			  					<button class="btn btn-default removeProductRowBtn" type="button" id="removeProductRowBtn" onclick="removeProductRow(<?php echo $x; ?>)"><i class="glyphicon glyphicon-trash"></i></button>
			  				</td>
			  			</tr>
		  			<?php
		  			$arrayNumber++;
		  			$x++;
			  		} // /for
			  		?>
			  	</tbody>			  	
			  </table>

			  <div class="col-md-6">
			  	<div class="form-group">
				    <label for="subTotal" class="col-sm-3 control-label">Sub total</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" id="subTotal" name="subTotal" disabled="true" value="<?php echo $data[4] ?>" />
				      <input type="hidden" class="form-control" id="subTotalValue" name="subTotalValue" value="<?php echo $data[4] ?>" />
				    </div>
				  </div> <!--/form-group-->			  
				  <div class="form-group">
				    <label for="vat" class="col-sm-3 control-label">IVA 12%</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" id="vat" name="vat" disabled="true" value="<?php echo $data[5] ?>"  />
				      <input type="hidden" class="form-control" id="vatValue" name="vatValue" value="<?php echo $data[5] ?>"  />
				    </div>
				  </div> <!--/form-group-->			  
				  <div class="form-group">
				    <label for="totalAmount" class="col-sm-3 control-label">Total neto</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" id="totalAmount" name="totalAmount" disabled="true" value="<?php echo $data[6] ?>" />
				      <input type="hidden" class="form-control" id="totalAmountValue" name="totalAmountValue" value="<?php echo $data[6] ?>"  />
				    </div>
				  </div> <!--/form-group-->			  
				  <div class="form-group">
				    <label for="discount" class="col-sm-3 control-label">Descuento</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" id="discount" name="discount" onkeyup="discountFunc()" autocomplete="off" value="<?php echo $data[7] ?>" />
				    </div>
				  </div> <!--/form-group-->	
				  <div class="form-group">
				    <label for="grandTotal" class="col-sm-3 control-label">Total</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" id="grandTotal" name="grandTotal" disabled="true" value="<?php echo $data[8] ?>"  />
				      <input type="hidden" class="form-control" id="grandTotalValue" name="grandTotalValue" value="<?php echo $data[8] ?>"  />
				    </div>
				  </div> <!--/form-group-->			  		  
			  </div> <!--/col-md-6-->

			  <div class="col-md-6">
			  	<div class="form-group">
				    <label for="paid" class="col-sm-4 control-label">Monto pagado</label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="paid" name="paid" autocomplete="off" onkeyup="paidAmount()" value="<?php echo $data[9] ?>"  />
				    </div>
				  </div> <!--/form-group-->			  
				  <div class="form-group">
				    <label for="due" class="col-sm-4 control-label">Saldo</label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="due" name="due" disabled="true" value="<?php echo $data[10] ?>"  />
				      <input type="hidden" class="form-control" id="dueValue" name="dueValue" value="<?php echo $data[10] ?>"  />
				    </div>
				  </div> <!--/form-group-->		
				  <div class="form-group">
				    <label for="clientContact" class="col-sm-4 control-label">Método de pago</label>
				    <div class="col-sm-8">
				      <select class="form-control" name="paymentType" id="paymentType" >
				      	<option value="">-- Selecciona --</option>
				      	<option value="1" <?php if($data[11] == 1) {
				      		echo "selected";
				      	} ?> >Cheque</option>
				      	<option value="2" <?php if($data[11] == 2) {
				      		echo "selected";
				      	} ?>  >Efectivo</option>
				      	<option value="3" <?php if($data[11] == 3) {
				      		echo "selected";
				      	} ?> >Tarjeta de crédito</option>
				      </select>
				    </div>
				  </div> <!--/form-group-->							  
				  <div class="form-group">
				    <label for="clientContact" class="col-sm-4 control-label">Estado</label>
				    <div class="col-sm-8">
				      <select class="form-control" name="paymentStatus" id="paymentStatus">
				      	<option value="">-- Selecciona --</option>
				      	<option value="1" <?php if($data[12] == 1) {
				      		echo "selected";
				      	} ?>  >Pago completo</option>
				      	<option value="2" <?php if($data[12] == 2) {
				      		echo "selected";
				      	} ?> >Pago por adelantado</option>
				      	<option value="3" <?php if($data[10] == 3) {
				      		echo "selected";
				      	} ?> >No pagado</option>
				      </select>
				    </div>
				  </div> <!--/form-group-->							  
			  </div> <!--/col-md-6-->


			  <div class="form-group editButtonFooter">
			    <div class="col-sm-offset-2 col-sm-10">
			    <button type="button" class="btn btn-default" onclick="addRow()" id="addRowBtn" data-loading-text="cargando..."> <i class="glyphicon glyphicon-plus-sign"></i> Añadir fila </button>

			    <input type="hidden" name="orderId" id="orderId" value="<?php echo $_GET['i']; ?>" />

			    <button type="submit" id="editOrderBtn" data-loading-text="Loading..." class="btn btn-success"><i class="glyphicon glyphicon-ok-sign"></i> Guardar cambios</button>
			      
			    </div>
			  </div>
			</form>

			<?php
		} // /get order else  ?>


	</div> <!--/panel-->	
</div> <!--/panel-->	


<!-- edit order -->
<div class="modal fade" tabindex="-1" role="dialog" id="paymentOrderModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="glyphicon glyphicon-edit"></i> Editar pago</h4>
      </div>      

      <div class="modal-body form-horizontal" style="max-height:500px; overflow:auto;" >

      	<div class="paymentOrderMessages"></div>

      	     				 				 
			  <div class="form-group">
			    <label for="due" class="col-sm-3 control-label">Saldo</label>
			    <div class="col-sm-9">
			      <input type="text" class="form-control" id="due" name="due" disabled="true" />					
			    </div>
			  </div> <!--/form-group-->		
			  <div class="form-group">
			    <label for="payAmount" class="col-sm-3 control-label">Monto a pagar</label>
			    <div class="col-sm-9">
			      <input type="text" class="form-control" id="payAmount" name="payAmount"/>					      
			    </div>
			  </div> <!--/form-group-->		
			  <div class="form-group">
			    <label for="clientContact" class="col-sm-3 control-label">Método de pago</label>
			    <div class="col-sm-9">
			      <select class="form-control" name="paymentType" id="paymentType" >
			      	<option value=""> -- Selecciona -- </option>
			      	<option value="1">Cheque</option>
			      	<option value="2">Efectivo</option>
			      	<option value="3">Tarjeta de crédito</option>
			      </select>
			    </div>
			  </div> <!--/form-group-->							  
			  <div class="form-group">
			    <label for="clientContact" class="col-sm-3 control-label">Estado</label>
			    <div class="col-sm-9">
			      <select class="form-control" name="paymentStatus" id="paymentStatus">
			      	<option value="">-- Selecciona --</option>
			      	<option value="1">Pago completo</option>
			      	<option value="2">Pago por adelantado</option>
			      	<option value="3">No pagado</option>
			      </select>
			    </div>
			  </div> <!--/form-group-->							  				  
      	        
      </div> <!--/modal-body-->
      <div class="modal-footer">
      	<button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Cerrar</button>
        <button type="button" class="btn btn-primary" id="updatePaymentOrderBtn" data-loading-text="Loading..."> <i class="glyphicon glyphicon-ok-sign"></i> Guardar cambios</button>	
      </div>           
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /edit order-->

<!-- remove order -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeOrderModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="glyphicon glyphicon-trash"></i> Eliminar venta</h4>
      </div>
      <div class="modal-body">

      	<div class="removeOrderMessages"></div>

        <p>Realmente deseas eliminar este registro?</p>
      </div>
      <div class="modal-footer removeProductFooter">
        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Cerrar</button>
        <button type="button" class="btn btn-primary" id="removeOrderBtn" data-loading-text="Loading..."> <i class="glyphicon glyphicon-ok-sign"></i> Guardar cambios</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /remove order-->


<script src="assests/plugins/select2/js/select2.js"></script>
<link rel="stylesheet" href="assests/plugins/select2/css/select2.css">
<script src="custom/js/order.js"></script>

<?php require_once 'includes/footer.php'; ?>


	
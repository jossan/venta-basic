-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 08, 2017 at 05:20 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos_stock`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `brand_active` int(11) NOT NULL DEFAULT '0',
  `brand_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_name`, `brand_active`, `brand_status`) VALUES
(1, 'Polo', 1, 1),
(2, 'jose', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `categories_id` int(11) NOT NULL,
  `categories_name` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `categories_active` int(11) NOT NULL DEFAULT '1',
  `categories_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`categories_id`, `categories_name`, `parent_id`, `categories_active`, `categories_status`) VALUES
(1, 'Camisas', 2, 1, 1),
(2, 'algoo', 0, 1, 1),
(4, 'grupoo', 5, 1, 1),
(5, 'grupo pade', 0, 1, 1),
(6, 'grupo padre 2', 0, 1, 1),
(7, 'jose', 0, 1, 1),
(8, 'fruta', 0, 1, 1),
(9, 'piña', 7, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellido` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cedula` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correo` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`id`, `nombre`, `apellido`, `cedula`, `telefono`, `correo`, `direccion`) VALUES
(1, 'jose luis', 'macias santanaaa', '1314538644', '1', '2', ''),
(2, 'new', '12qw', 'sadxz', '1', '2', '3'),
(6, 'pedro ', 'rodriguez', '123456', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `order_date` date NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `client_contact` varchar(255) NOT NULL,
  `sub_total` varchar(255) NOT NULL,
  `vat` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `discount` varchar(255) NOT NULL,
  `grand_total` varchar(255) NOT NULL,
  `paid` varchar(255) NOT NULL,
  `due` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL,
  `order_status` int(11) NOT NULL DEFAULT '0',
  `factura` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_date`, `client_name`, `client_contact`, `sub_total`, `vat`, `total_amount`, `discount`, `grand_total`, `paid`, `due`, `payment_type`, `payment_status`, `order_status`, `factura`) VALUES
(1, '2017-01-22', 'Juan Campos', '+(503) 7025-25200', '17.99', '2.34', '20.33', '0', '20.33', '20.33', '0.00', 2, 1, 1, ''),
(2, '2017-07-31', '1', '', '123.00', '14.76', '137.76', '0', '137.76', '123', '14.76', 2, 1, 1, ''),
(3, '2017-07-31', '1', '', '123.00', '14.76', '137.76', '0', '137.76', '123', '14.76', 2, 1, 1, ''),
(4, '2017-07-31', '2', '', '123', '14.76', '137.76', '0', '137.76', '2', '135.76', 2, 1, 1, ''),
(5, '2017-08-01', '1', '', '246', '29.52', '275.52', '0', '275.52', '123', '152.52', 2, 1, 1, ''),
(6, '2017-08-02', '1', '', '123', '14.76', '137.76', '0', '137.76', '137.76', '0', 2, 1, 1, ''),
(7, '2017-08-02', '1', '', '5', '0.6', '5.6', '0', '5.6', '5.60', '0', 2, 1, 1, ''),
(8, '2017-08-08', '6', '', '251', '30.12', '281.12', '0', '281.12', '281.12', '0', 2, 1, 1, ''),
(9, '2017-08-08', '1', '', '246', '29.52', '275.52', '0', '275.52', '137.76', '137.76', 2, 1, 1, ''),
(10, '2017-08-08', '1', '', '5', '0.6', '5.6', '0', '5.6', '5.60', '0', 2, 1, 1, 'joseluis');

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `order_item_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `quantity` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `total` varchar(255) NOT NULL,
  `order_item_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`order_item_id`, `order_id`, `product_id`, `quantity`, `rate`, `total`, `order_item_status`) VALUES
(1, 1, 1, '1', '17.99', '17.99', 1),
(2, 2, 3, '1', '123', '123.00', 1),
(3, 3, 3, '1', '123', '123.00', 1),
(4, 0, 3, '1', '123', '123.00', 1),
(5, 4, 3, '1', '', '123', 1),
(6, 5, 3, '1', '123', '123', 1),
(7, 5, 4, '1', '123', '123', 1),
(8, 6, 4, '1', '123', '123', 1),
(9, 7, 8, '1', '5', '5', 1),
(10, 8, 3, '1', '123', '123', 1),
(11, 8, 4, '1', '123', '123', 1),
(12, 8, 8, '1', '5', '5', 1),
(13, 9, 3, '2', '123', '246', 1),
(14, 10, 8, '1', '5', '5', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_image` text NOT NULL,
  `brand_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `marca` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `referencia` text NOT NULL,
  `stockminimo` int(11) NOT NULL,
  `preciocompra` float(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `product_image`, `brand_id`, `categories_id`, `quantity`, `rate`, `active`, `status`, `marca`, `descripcion`, `referencia`, `stockminimo`, `preciocompra`) VALUES
(1, 'Camisa formal para hombre', '../assests/images/stock/246865885413162f4c.jpg', 2, 1, '2', '17.99', 0, 1, '', '', '', 1, 0.00),
(2, 'nombre', '../assests/images/stock/292873129597c265673e28.jpg', 2, 3, '10', '3', 0, 1, '', '4', '8', 9, 2.00),
(3, 's', '../assests/images/stock/1399021923597d586ba567b.png', 1, 1, '7', '123', 1, 1, '', '', '', 1, 0.00),
(4, 'poloo', '../assests/images/stock/1613802945597dde98d85ac.png', 2, 1, '13', '123', 1, 1, '', '', '', 0, 123.00),
(5, 'producto', '', 2, 2, '17', '1234566', 2, 2, '', '121', '', 0, 12.00),
(6, 'zapatos', '', 2, 1, '2', '555', 2, 2, '', 'nose', '', 4, 444.00),
(7, 'tacho', '', 1, 2, '12', '2.00', 2, 2, '', 'inalesa', 'referencia', 5, 1.40),
(8, 'zapatos', '', 1, 2, '0', '5.00', 1, 1, 'nike', 'nike', 'referenciaa', 5, 3.40);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `email`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categories_id`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`order_item_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `categories_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `order_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
